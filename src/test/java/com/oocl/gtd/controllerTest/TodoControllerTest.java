package com.oocl.gtd.controllerTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oocl.gtd.entity.Todo;
import com.oocl.gtd.repository.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @Autowired
    private MockMvc client;

    @Autowired
    private TodoRepository todoRepository;

    @BeforeEach
    void beforeEach() {
        todoRepository.deleteAll();
    }

    @Test
    void should_return_todos_perform_get_given_todos_in_db() throws Exception {
        // given
        Todo todo1 = new Todo("read book", false);
        Todo todo2 = new Todo("do sports", false);
        todoRepository.saveAll(List.of(todo1, todo2));

        // when then
        client.perform(MockMvcRequestBuilders.get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text").value(todo1.getText()))
                .andExpect(jsonPath("$[0].done").value(todo1.getDone()))
                .andExpect(jsonPath("$[1].text").value(todo2.getText()))
                .andExpect(jsonPath("$[1].done").value(todo2.getDone()));
    }

    @Test
    void should_add_todo_perform_post_given_todo() throws Exception {
        // given
        Todo todo = new Todo("read book", false);
        ObjectMapper objectMapper = new ObjectMapper();
        String requestJson = objectMapper.writeValueAsString(todo);

        // when then
        client.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.text").value(todo.getText()))
                .andExpect(jsonPath("$.done").value(todo.getDone()));
    }

    @Test
    void should_delete_todo_perform_delete_given_todo() throws Exception {
        // given
        Todo todo = new Todo("read book", false);
        Todo save = todoRepository.save(todo);

        // when
        client.perform(delete("/todos/{id}", save.getId()));

        // then
        assertTrue(todoRepository.findById(save.getId()).isEmpty());
    }


    @Test
    void should_update_todo_perform_put_given_todo() throws Exception {
        // given
        Todo todo = new Todo("read book", false);
        Todo save = todoRepository.save(todo);
        Todo updateTodo = new Todo("do sport", false);
        ObjectMapper objectMapper = new ObjectMapper();
        String requestJson = objectMapper.writeValueAsString(updateTodo);

        // when
        client.perform(put("/todos/{id}", save.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));

        // then
        Todo updatedTodo = todoRepository.findById(save.getId()).orElseThrow();
        assertEquals(updateTodo.getText(), updatedTodo.getText());
        assertEquals(updateTodo.getDone(), updatedTodo.getDone());
    }

    @Test
    void should_get_todo_by_id_perform_get_by_id_given_todo() throws Exception {
        // given
        Todo todo = new Todo("read book", false);
        Todo save = todoRepository.save(todo);

        // when
        // then
        client.perform(get("/todos/{id}", save.getId()))
                .andExpect(jsonPath("$.id").value(save.getId()))
                .andExpect(jsonPath("$.text").value(save.getText()))
                .andExpect(jsonPath("$.done").value(save.getDone()));
    }

    @Test
    void should_error_perform_get_by_id_given_todo() throws Exception {
        // given
        Todo todo = new Todo("read book", false);
        Todo save = todoRepository.save(todo);

        // when
        // then
        client.perform(get("/todos/{id}", save.getId() + 1))
                .andExpect(status().isNotFound());
    }
}
