package com.oocl.gtd.mapper;

import com.oocl.gtd.dto.UpdateCreateTodo;
import com.oocl.gtd.entity.Todo;
import org.springframework.beans.BeanUtils;

public class TodoMapper {

    public static Todo toEntity(UpdateCreateTodo updateTodo) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(updateTodo, todo);
        return todo;
    }
}
