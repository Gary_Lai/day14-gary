package com.oocl.gtd.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UpdateCreateTodo {

    private String text;
    private Boolean done;
}
