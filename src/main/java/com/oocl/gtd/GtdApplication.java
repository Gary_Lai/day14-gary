package com.oocl.gtd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GtdApplication {

	public static void main(String[] args) {
		SpringApplication.run(GtdApplication.class, args);
		System.out.println("O God Start!");
	}

}
