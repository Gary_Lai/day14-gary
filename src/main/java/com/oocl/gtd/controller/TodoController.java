package com.oocl.gtd.controller;

import com.oocl.gtd.dto.UpdateCreateTodo;
import com.oocl.gtd.entity.Todo;
import com.oocl.gtd.service.TodoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<Todo> getAllTodos() {
        return todoService.getAllTodos();
    }

    @GetMapping("/{id}")
    public Todo getTodoById(@PathVariable Integer id) {
        return todoService.getTodoById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Todo addTodo(@RequestBody UpdateCreateTodo createTodo) {
        return todoService.addTodo(createTodo);
    }

    @DeleteMapping("/{id}")
    public void deleteTodo(@PathVariable Integer id) {
        todoService.deleteById(id);
    }

    @PutMapping("/{id}")
    public void updateTodo(@PathVariable Integer id, @RequestBody UpdateCreateTodo updateTodo) {
        todoService.updateTodoById(id, updateTodo);
    }
}
