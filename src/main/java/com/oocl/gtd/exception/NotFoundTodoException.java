package com.oocl.gtd.exception;

public class NotFoundTodoException extends RuntimeException {
    public NotFoundTodoException() {
        super("Todo id not found");
    }

}
