package com.oocl.gtd.service;

import com.oocl.gtd.dto.UpdateCreateTodo;
import com.oocl.gtd.entity.Todo;
import com.oocl.gtd.exception.NotFoundTodoException;
import com.oocl.gtd.repository.TodoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.oocl.gtd.mapper.TodoMapper.toEntity;

@Service
public class TodoService {

    private final TodoRepository todoRepository;


    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<Todo> getAllTodos() {
        return todoRepository.findAll();
    }


    public Todo addTodo(UpdateCreateTodo createTodo) {
        return todoRepository.save(toEntity(createTodo));
    }

    public void deleteById(Integer id) {
        todoRepository.deleteById(id);
    }

    public void updateTodoById(Integer id, UpdateCreateTodo updateTodo) {
        Todo todo = toEntity(updateTodo);
        todo.setId(id);
        todoRepository.save(todo);
    }

    public Todo getTodoById(Integer id) {
        return todoRepository.findById(id).orElseThrow(NotFoundTodoException::new);
    }
}
