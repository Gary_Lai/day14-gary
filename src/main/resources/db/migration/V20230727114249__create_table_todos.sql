drop table if exists `todo`;

create table `todo` (
    `id` int(7) primary key auto_increment not null,
    `text` varchar(255),
    `done` tinyint(1)
);