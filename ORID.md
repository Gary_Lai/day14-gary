##  Daily Summary 

**Time: 2023/7/27**

**Author: 赖世龙**

---

**O (Objective):**  Today, I learned todo's back-end replacement mockApi, what is cross-domain, and how to solve cross-domain. The session performance in the afternoon was not very good, and the understanding of User Journey was not deep enough

**R (Reflective):**  Satisfied

**I (Interpretive):**  Configuring webconfig to solve cross-domain is very simple compared with front-end cross-domain solution, so this solution needs more learning and understanding.

**D (Decisional):**  I can configure webmvcconfig myself later on in the project to solve cross-domain problems

